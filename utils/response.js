var STATUS = require('../config').statusCodes;

module.exports.success = success;
module.exports.fail = fail;

function success(res, data, status) {
    if (!status) {
        status = STATUS.ok;
    }

    if (!data) {
        data = {
            ok: true
        };
    }

    res.status(status);
    res.json(data);

    return Promise.resolve();
}

function fail(res, message, status) {
    var response = {
        status: status,
        message: message
    };
    return Promise.reject(response);
}
