var mongoose = require('../database').Mongoose;
var Schema = mongoose.Schema;

module.exports.schema = schema;
module.exports.model = model;
module.exports.id = id;
module.exports.date = date;

function schema(obj) {
    return new Schema(obj, {
        versionKey: false
    });
}

function model(name, schema) {
    return mongoose.model(name, schema);
}

function id(ref) {
    return {
        type: Schema.ObjectId,
        ref: ref
    };
}

function date() {
    return {
        type: Date,
        default: Date.now
    };
}
