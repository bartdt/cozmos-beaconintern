module.exports = {
    statusCodes: {
        ok: 200,
        created: 201,
        badrequest: 400,    // queryparams/body
        unauthorized: 401,  // user
        forbidden: 403,     // appVersion
        notfound: 404,
        error: 500
    },
    supportedAppVersions: [
        '1.0'
    ],
    apiKeys: {
        gcm: 'AIzaSyBgoDkfibYenJoqMQhS3hQEzn_OgFZn7OM'
    }
};
