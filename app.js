var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');

var api = require('./routes/api/index');
var admin = require('./routes/admin/index');
var signup = require('./routes/signup');

var SUPPORTED_APP_VERSIONS = require('./config').supportedAppVersions;
var STATUS = require('./config').statusCodes;

var app = express();

// environment
app.set('env', 'development');
app.use(logger('dev'));
app.use(bodyParser.json());                             // parse application/json
app.use(bodyParser.urlencoded({ extended: false }));    // parse application/x-www-form-urlencoded

app.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Content-Type, appVersion");
    next();
});

app.options('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Methods", "PUT, DELETE");
    res.sendStatus(200);
});

// checks if app is up-to-date
app.use(appVersionValidator);

// routes
app.use('/api', api);
app.use('/admin', admin);
app.use('/signup', signup);

// no request handler
app.use(function (req, res, next) {
    var error = {
        status: STATUS.notfound,
        message: 'Not found.'
    };
    next(error);
});

// error handler
if (app.get('env') === 'development') {
    app.use(devErrorHandler);
} else {
    app.use(proErrorHandler);
}

// start
app.listen(2410);

module.exports = app;

function appVersionValidator(req, res, next) {
    if (req.url.startsWith('/api')) {
        var appVersion = req.header('appVersion');
        var index = SUPPORTED_APP_VERSIONS.indexOf(appVersion);

        if (index < 0) {
            var error = {
                status: STATUS.forbidden,
                message: 'App version not supported.'
            };
            next(error);
        }
    }

    next();
}

function devErrorHandler(err, req, res, next) {
    res.status(err.status || STATUS.error);
    res.json({
        message: err.message,
        stack: err.stack
    });
}

function proErrorHandler(err, req, res, next) {
    res.status(err.status || STATUS.error);
    res.json({
        message: err.message
    });
}
