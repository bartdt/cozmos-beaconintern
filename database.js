var Mongoose = require('mongoose');
var MONGO_AUTH = require('./authentications').mongo;

Mongoose.connect('mongodb://' + MONGO_AUTH.username + ':' + MONGO_AUTH.password + '@' + MONGO_AUTH.url + '/' + MONGO_AUTH.database);
var db = Mongoose.connection;

db.on('error', console.error.bind(console, 'Database connection error!'));
db.once('open', function () {
    console.log("Connection with database succeeded.");
});

exports.Mongoose = Mongoose;
exports.db = db;
