var express = require('express');
var router = express.Router();

var moment = require('moment');

var User = require('../../models/User');
var Enquete = require('../../models/Enquete');

var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/enquetes', getEnquetes);

    router.get('/enquetes/:id', getEnquete)
    router.put('/enquetes/:id', vote);
}


/*
URL:    /enquetes
METHOD: get
PARAMS: -
QUERY:  deviceId, startDate ('DD/MM/YYYY'), endDate ('DD/MM/YYYY')
BODY:   -
RETURN: Array
[
    {
        id: String,
        title: String,
        description: String,
        startDate: Date,
        endDate: Date,
        myVote: Boolean
    }
]
*/
function getEnquetes(req, res, next) {
    var deviceId = req.query.deviceId;

    var startDate = moment().startOf('day');
    var endDate = moment().endOf('day');

    if (req.query.startDate) {
        startDate = moment(req.query.startDate, 'DD/MM/YYYY').startOf('day');
    }

    if (req.query.endDate) {
        endDate = moment(req.query.endDate, 'DD/MM/YYYY').endOf('day');
    }

    if (endDate.diff(startDate) < 0) {
        startDate = moment(req.query.startDate, 'DD/MM/YYYY').endOf('day');
    }

    var myUserId;

    findUserId(deviceId, res)
        .then(function (id) {
            myUserId = id;

            return Enquete.find({
                    startDate: { $lte: endDate.toDate() },
                    endDate: { $gte: startDate.toDate() }
                })
        })
        .then(function (enquetes) {
            var data = enquetes.map(function (enquete, myUserId) {
                return convertEnquete(enquete);
            });

            return Response.success(res, data);
        })
        .then(null, next);
}

function convertEnquete(enquete, userId) {
    var myVote = false;

    enquete.values.forEach(function (value) {
        var index = value.votes.indexOf(userId);
        if (index >= 0) {
            myVote = true;
        }
    });

    return {
        id: enquete._id,
        title: enquete.title,
        description: enquete.description,
        startDate: enquete.startDate,
        endDate: enquete.endDate,
        myVote: myVote
    }
}

function findUserId(deviceId, res) {
    var msg = 'Invalid deviceId.';

    if (!deviceId) {
        return Response.fail(res, msg, STATUS.badrequest);
    }

    return User.findOne({ 'device.id': deviceId })
        .then(function (user) {
            if (!user) {
                return Response.fail(res, msg, STATUS.notfound);
            }

            return user._id;
        });
}


/*
URL:    /enquetes/:id
METHOD: get
PARAMS: id
QUERY:  -
BODY:   -
RETURN: Object
{
    id: String,
    title: String,
    description: String,
    extra: String,
    startDate: Date,
    endDate: Date,
    values: [
        name: String,
        votes: Number,
        myVote: Boolean
    ],
    open: Boolean
}
*/
function getEnquete(req, res, next) {
    var id = req.params.id;
    var deviceId = req.query.deviceId;

    var myUserId;

    findUserId(deviceId, res)
        .then(function (userId) {
            myUserId = userId;
            return Enquete.findById(id);
        })
        .then(function (enquete) {
            if (!enquete) {
                var msg = 'Enquete not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            var data = convertEnqueteDetails(enquete, myUserId);
            return Response.success(res, data);
        })
        .then(null, next);
}

function convertEnqueteDetails(enquete, myUserId) {
    var values = enquete.values.map(function (value) {
        var index = value.votes.indexOf(myUserId);

        return {
            name: value.name,
            votes: value.votes.length,
            myVote: (index >= 0)
        };
    });

    return {
        id: enquete._id,
        title: enquete.title,
        description: enquete.description,
        extra: enquete.extra,
        startDate: enquete.startDate,
        endDate: enquete.endDate,
        values: values,
        open: enquete.open
    };
}


/*
URL:    /enquetes/:id
METHOD: put
PARAMS: id
QUERY:  -
BODY:   Object
{
    deviceId: String,
    valueId: String,
    valueName: String
}
RETURN: Object
{
    id: String,
    title: String,
    description: String,
    extra: String,
    startDate: Date,
    endDate: Date,
    values: [
        name: String,
        votes: Number,
        myVote: Boolean
    ],
    open: Boolean
}
*/
function vote(req, res, next) {
    var id = req.params.id;
    var deviceId = req.body.deviceId;
    var valueId = req.body.valueId;
    var valueName = req.body.valueName;

    var myUserId;

    findUserId(deviceId, res)
        .then(function (userId) {
            myUserId = userId;
            return Enquete.findById(id);
        })
        .then(function (enquete) {
            if (!enquete) {
                var msg = 'Enquete not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            var valueIndex = removePreviousVoteAndGetNewIndex(enquete, myUserId, valueId);

            if (valueIndex >= 0) {
                enquete.values[valueIndex].votes.push(myUserId);
            } else {
                if (enquete.open && valueName) {
                    addValueAndVote(enquete, myUserId, valueName);
                } else {
                    var msg = 'Unable to add value to enquete.';
                    return Response.fail(res, msg, STATUS.badrequest);
                }
            }

            return enquete.save();
        })
        .then(function (enquete) {
            var data = convertEnqueteDetails(enquete, myUserId);
            return Response.success(res, data);
        })
        .then(null, next);
}

function removePreviousVoteAndGetNewIndex(enquete, myUserId, valueId) {
    var valueIndex = -1;

    for (var i = 0; i < enquete.values.length; i++) {
        var value = enquete.values[i];

        if (value._id == valueId) {
            valueIndex = i;
        }

        var index = value.votes.indexOf(myUserId);
        if (index >= 0) {
            value.votes.splice(index, 1);
        }
    }

    return valueIndex;
}

function addValueAndVote(enquete, myUserId, valueName) {
    var index;
    for (var i = 0; i < enquete.values.length; i++) {
        var value = enquete.values[i];
        if (value.name.toLowerCase() === valueName.toLowerCase()) {
            index = i;
        }
    }

    if (index >= 0) {
        enquete.values[index].votes.push(myUserId);
    } else {
        index = enquete.values.length;
        var value = {
            name: valueName,
            votes: []
        };

        enquete.values.push(value);
        enquete.values[index].votes.push(myUserId);
    }
}
