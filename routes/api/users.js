var express = require('express');
var router = express.Router();

var Beacon = require('../../models/Beacon');
var User = require('../../models/User');

var ObjectId = require('mongoose').Types.ObjectId;
var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;
var MAIL_AUTH = require('../../authentications').mail;

module.exports = function (router) {
    router.get('/users/login', login);
    router.post('/users/register', register);

    router.put('/users/update/device', updateDevice);
    router.put('/users/update/email', updateEmail);
    router.put('/users/update/token', updateToken);
}


/*
URL:    /users/login
METHOD: get
PARAMS: -
QUERY:  deviceId
BODY:   -
RETURN: Object
{
    regions: [ String ]
}
*/
function login(req, res, next) {
    var deviceId = req.query.deviceId;

    User.findOne({ 'device.id': deviceId })
        .then(function (user) {
            if (!user) {
                var msg = 'User not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            if (!user.device.registrated) {
                var msg = 'User not confirmed.';
                return Response.fail(res, msg, STATUS.unauthorized);
            }

            return Beacon.distinct('uuid');
        })
        .then(function (uuids) {
            var data = {
                regions: uuids
            };
            return Response.success(res, data);
        });
}


/*
URL:    /users/register
METHOD: post
PARAMS: -
QUERY:  -
BODY:   Object
{
    deviceId: String,
    email: String
    os: String,
    token: String
}
RETURN: ok
*/
function register(req, res, next) {
    var deviceId = req.body.deviceId;
    var email = req.body.email;
    var os = req.body.os;
    var token = req.body.token;

    if (!email || !os || !token) {
        var msg = 'Invalid body.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    if (!isEmailValid(email)) {
        var msg = 'Email invalid.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    User.findOne({ email: email })
        .then(function (user) {
            if (user) {
                var msg = 'Email already in use.';
                return Response.fail(res, msg, STATUS.unauthorized);
            }

            return User.findOne({ 'device.id': deviceId });
        })
        .then(function (user) {
            if (user) {
                var msg = 'Device already in use.';
                return Response.fail(res, msg, STATUS.unauthorized);
            }

            user = new User({
                device: {
                    id: deviceId,
                    registrated: false,
                    os: os,
                    token: token
                },
                email: email
            });

            return user.save();
        })
        .then(function (newUser) {
            var link = 'http://' + req.get('host') + '/signup?deviceId=' + newUser.device.id + '&email=' + newUser.email;
            sendEmail(link, newUser.email);

            return Response.success(res);
        })
        .then(null, next);
}

function isEmailValid(email) {
    var emailRegex = /[a-zA-Z]+\.[a-zA-Z]+@cozmos\.be/;
    return emailRegex.test(email);
}

function sendEmail(link, sendTo) {
    var email = require('emailjs');

    var server = email.server.connect({
        user: MAIL_AUTH.username,
        password: MAIL_AUTH.password,
        host: MAIL_AUTH.host,
        ssl: true
    });

    server.send({
        text: 'Klik op de volgende link om te bevestigen: ' + link,
        from: 'Bart De Tandt',
        to: sendTo,
        cc: '',
        subject: 'Bevestig je device - Cozmos App'
    }, function (err, message) {
        console.log(err || message);
    });
}


/*
URL:    /users/update/device
METHOD: put
PARAMS: -
QUERY:  -
BODY:   Object
{
    deviceId: String,
    email: String,
    os: String,
    token: String
}
RETURN: ok
*/
function updateDevice(req, res, next) {
    var deviceId = req.body.deviceId;
    var email = req.body.email;
    var os = req.body.os;
    var token = req.body.token;

    if (!email || !os || !token) {
        var msg = 'Invalid body.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    if (!isEmailValid(email)) {
        var msg = 'Email invalid.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    User.findOne({ email: email })
        .then(function (user) {
            if (!user) {
                var msg = 'User does not exists.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            user.device.id = deviceId;
            user.device.registrated = false;
            user.device.os = os;
            user.device.token = token;

            return user.save();
        })
        .then(function (updatedUser) {
            var link = 'http://' + req.get('host') + '/signup?deviceId=' + updatedUser.device.id + '&email=' + updatedUser.email;
            sendEmail(link, updatedUser.email);

            return Response.success(res);
        })
        .then(null, next);
}


/*
URL:    /users/update/email
METHOD: put
PARAMS: -
QUERY:  -
BODY:   Object
{
    deviceId: String,
    email: String
}
RETURN: ok
*/
function updateEmail(req, res, next) {
    var deviceId = req.body.deviceId;
    var email = req.body.email;

    if (!isEmailValid(email)) {
        var msg = 'Email invalid.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    User.findOne({ 'device.id': deviceId })
        .then(function (user) {
            if (!user) {
                var msg = 'User does not exists.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            if (user.device.registrated) {
                var msg = 'Unable to change email.';
                return Response.fail(res, msg, STATUS.unauthorized);
            }

            user.email = email;
            user.device.registrated = false;

            return user.save();
        })
        .then(function (updatedUser) {
            var link = 'http://' + req.get('host') + '/signup?deviceId=' + updatedUser.device.id + '&email=' + updatedUser.email;
            sendEmail(link, updatedUser.email);

            return Response.success(res);
        })
        .then(null, next);
}


/*
URL:    /users/update/token
METHOD: put
PARAMS: -
QUERY:  -
BODY:   Object
{
    deviceId: String,
    token: String
}
RETURN: ok
*/
function updateToken(req, res, next) {
    var deviceId = req.body.deviceId;
    var token = req.body.token;

    if (!token) {
        var msg = 'Invalid body.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    User.findOne({ 'device.id': deviceId })
        .then(function (user) {
            if (!user) {
                var msg = 'User does not exists.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            user.device.token = token;

            return user.save();
        })
        .then(function (updatedUser) {
            return Response.success(res);
        })
        .then(null, next);
}
