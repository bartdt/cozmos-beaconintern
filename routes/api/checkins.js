var express = require('express');
var router = express.Router();
var moment = require('moment');

var Beacon = require('../../models/Beacon');
var User = require('../../models/User');
var UserEntry = require('../../models/UserEntry');

var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/checkins', getUsersLastCheckins);
    router.post('/checkins', checkin);
}


/*
URL:    /checkins
METHOD: get
PARAMS: -
QUERY:  -
BODY:   -
RETURN: Array
[
    {
        user: {
            id: String,
            name: String
        },
        location: {
            id: String,
            name: String
        },
        time: Date,
        in: Boolean
    }
]
*/
function getUsersLastCheckins(req, res, next) {
    UserEntry.aggregate()
        .sort({
            time: -1
        })
        .group({
            _id: '$user',
            time: { $first: '$time'},
            in: { $first: '$in'},
            beacon: { $first: '$beaconId'},
        })
        .exec()
    .then(function (checkins) {
        return User.populate(checkins, { path: '_id' });
    })
    .then(function (checkins) {
        return Beacon.populate(checkins, { path: 'beacon' });
    })
    .then(function (checkins) {
        var data = convertCheckins(checkins);
        return Response.success(res, data);
    })
    .then(null, next);
}

function convertCheckins(data) {
    return data.filter(function (item) {
        if (item._id) {
            return item;
        }
    })
    .map(function (item) {
        var name = item._id.email.split('@')[0];
        return {
            user: {
                id : item._id._id,
                name : name
            },
            location: {
                id: item.beacon._id,
                name: item.beacon.name
            },
            time: item.time,
            in: item.in
        };
    });
}


/*
URL:    /checkins
METHOD: post
PARAMS: -
QUERY:  -
BODY:   Object
{
    deviceId: String,
    uuid: String,
    major: Number,
    minor: Number
    in: Boolean
}
RETURN: Object
{
    location: String
}
*/
function checkin(req, res, next) {
    var deviceId = req.body.deviceId;
    var uuid = req.body.uuid;
    var major = req.body.major;
    var minor = req.body.minor;
    var inParam = req.body.in;

    if (!deviceId || !uuid || !major || !minor || (inParam == undefined)) {
        var msg = 'Invalid body to check in.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    var beaconId;
    var beaconName;
    var user;

    // NOTE: We're using a regex with case insensitive search because Android sends the UUID in lowercase and iOS sends it in uppercase.
    Beacon.findOne({
            uuid: {
                $regex: new RegExp("^" + req.body.uuid + "$", "i")
            },
            major: req.body.major,
            minor: req.body.minor
        })
        .then(function (beacon) {
            if (!beacon) {
                var msg = 'Beacon not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            beaconId = beacon._id + "";
            beaconName = beacon.name;

            return User.findOne({ 'device.id': deviceId });
        })
        .then(function (foundUser) {
            user = foundUser;

            if (!user) {
                var msg = 'User not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            if (!user.device.registrated) {
                var msg = 'Email not confirmed.';
                return Response.fail(res, msg, STATUS.unauthorized);
            }

            return UserEntry.findOne({ user: user._id })
                .sort({
                    time: -1
                })
                .exec();
        })
        .then(function (lastUserEntry) {
            if (lastUserEntry) {
                if (lastUserEntry.beaconId == beaconId && lastUserEntry.in == inParam) {
                    var data = {
                        location: beaconName
                    };
                    return Response.success(res, data);
                }
            }

            var date = moment();

            var userentry = new UserEntry({
                in: inParam,
                user: user,
                beaconId: beaconId
            });

            return userentry.save()
                .then(function (data) {
                    var data = {
                        location: beaconName
                    };
                    return Response.success(res, data);
                });
        })
        .then(null, next);
}
