var express = require('express');
var router = express.Router();

require('./checkins')(router)
require('./enquetes')(router)
require('./templates')(router)
require('./users')(router)

module.exports = router;
