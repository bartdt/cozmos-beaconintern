var express = require('express');
var router = express.Router();

var Beacon = require('../../models/Beacon');
var Template = require('../../models/Template');

var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/templates', getTemplates);

    router.get('/templates/:id', getTemplate);
}


/*
URL:    /templates
METHOD: get
PARAMS: -
QUERY:  -
BODY:   -
RETURN: Array
[
    {
        id: String,
        title: String,
        description: String
    }
]
*/
function getTemplates(req, res, next) {
    Template.find()
        .then(function (templates) {
            templates = convertTemplates(templates);
            return Response.success(res, templates);
        })
        .then(null, next);
}

function convertTemplates(templates) {
    return templates.map(function (temp) {
        var len = temp.locations.length;
        return {
            id: temp._id,
            title: temp.title,
            description: temp.description
        }
    });
}


/*
URL:    /templates/:id
METHOD: get
PARAMS: id
QUERY:  -
BODY:   -
RETURN: Object
{
    id: String,
    title: String,
    description: String,
    extra: String,
    values: [ String ],
    locations: [
        {
            id: String,
            name: String,
            selected: Boolean
        }
    ]
}
*/
function getTemplate(req, res, next) {
    var id = req.params.id;
    var temp;

    Template.findById(id)
        .then(function (template) {
            if (!template) {
                var msg = 'Template not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            temp = template;
            return Beacon.find();
        })
        .then(function (beacons) {
            var template = convertTemplate(temp, beacons);
            return Response.success(res, template);
        })
        .then(null, next);
}

function convertTemplate(template, beacons) {
    var bs = beacons.map(function (beacon) {
        var index = 0;
        if (template.locations.length > 0) {
            index = template.locations.indexOf(beacon._id);
        }

        return {
            id: beacon._id,
            name: beacon.name,
            selected: (index >= 0)
        };
    });

    return {
        id: template._id,
        title: template.title,
        description: template.description,
        extra: template.extra,
        values: template.values,
        locations: bs
    };
}
