var express = require('express');
var router = express.Router();
var moment = require('moment');
var schedule = require('node-schedule');

var Beacon = require('../../models/Beacon');
var Enquete = require('../../models/Enquete');
var Template = require('../../models/Template');

var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/enquetes', getEnquetes);
    router.post('/enquetes', createEnquete);

    router.get('/enquetes/:id', getEnquete);
    router.put('/enquetes/:id', updateEnquete);
    router.delete('/enquetes/:id', deleteEnquete);

    var dayOfWeek = 5;
    var hour = 1;
    var minute = 0;
    var second = 0;
    schedule.scheduleJob(second + ' ' + minute + ' ' + hour + ' * * ' + dayOfWeek, createFridayEnquete);
}


/*
URL:    /enquetes
METHOD: get
PARAMS: -
QUERY:  startDate ('DD/MM/YYYY'), endDate ('DD/MM/YYYY')
BODY:   -
RETURN: Array
[
    {
        id: String,
        title: String,
        description: String,
        startDate: Date,
        endDate: Date
    }
]
*/
function getEnquetes(req, res, next) {
    var startDate = moment().startOf('day');
    var endDate = moment().endOf('day');

    if (req.query.startDate) {
        startDate = moment(req.query.startDate, 'DD/MM/YYYY').startOf('day');
    }

    if (req.query.endDate) {
        endDate = moment(req.query.endDate, 'DD/MM/YYYY').endOf('day');
    }

    if (endDate.diff(startDate) < 0) {
        startDate = moment(req.query.startDate, 'DD/MM/YYYY').endOf('day');
    }

    Enquete.find({
            startDate: { $lte: endDate.toDate() },
            endDate: { $gte: startDate.toDate() }
        })
        .then(function (enquetes) {
            var data = enquetes.map(function (enquete) {
                return convertEnquete(enquete);
            });

            return Response.success(res, data);
        })
        .then(null, next);
}

function convertEnquete(enquete) {
    return {
        id: enquete._id,
        title: enquete.title,
        description: enquete.description,
        startDate: enquete.startDate,
        endDate: enquete.endDate
    }
}


/*
URL:    /enquetes
METHOD: post
PARAMS: -
QUERY:  -
BODY:   Object
{
    title: String,
    description: String,
    extra: String,
    startDate: String ('DD/MM/YYYY HH:mm'),
    endDate: String ('DD/MM/YYYY HH:mm'),
    values: [ String ],
    locations: [ String ],
    open: Boolean
}
RETURN: status
*/
function createEnquete(req, res, next) {
    var title = req.body.title;
    var description = req.body.description;
    var extra = req.body.extra;
    var startDate = moment(req.body.startDate, 'DD/MM/YYYY HH:mm');
    var endDate = moment(req.body.endDate, 'DD/MM/YYYY HH:mm');
    var values = req.body.values;
    var locations = req.body.locations;
    var open = req.body.open || false ;

    if (!title || !description || !startDate || !endDate || !values) {
        var msg = 'Invalid body to create enquete.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    if (endDate.diff(startDate) < 0) {
        var msg = 'Invalid body to create enquete.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    if (values.length < 2) {
        var msg = 'Invalid body to create enquete.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    var enqueteValues = values.map(function (value) {
        return {
            name: value,
            votes: []
        };
    });

    var enquete = new Enquete({
        title: title,
        description: description,
        extra: extra,
        startDate: startDate,
        endDate: endDate,
        values: enqueteValues,
        locations: locations,
        open: open
    });

    enquete.save()
        .then(function (newEnquete) {
            return Response.success(res, null, STATUS.created);
        })
        .then(null, next);
}


/*
URL:    /enquetes/:id
METHOD: get
PARAMS: id
QUERY:  -
BODY:   -
RETURN: Object
{
    id: String,
    title: String,
    description: String,
    extra: String,
    startDate: Date,
    endDate: Date,
    locations: [
        id: String,
        name: String,
        selected: Boolean
    ],
    values: [
        name: String,
        votes: Number
    ]
}
*/
function getEnquete(req, res, next) {
    var id = req.params.id;

    var thisEnquete;
    var locations = [];

    Enquete.findById(id)
        .then(function (enquete) {
            if (!enquete) {
                var msg = 'Enquete not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            thisEnquete = enquete;
            return Beacon.find();
        })
        .then(function (beacons) {
            beacons.forEach(function (beacon) {
                var index = thisEnquete.locations.indexOf(beacon._id);

                var loc = {
                    id: beacon._id,
                    name: beacon.name,
                    selected: (index >= 0)
                };

                locations.push(loc);
            });

            var values = thisEnquete.values.map(function (value) {
                return {
                    name: value.name,
                    votes: value.votes.length
                };
            });

            var data = {
                id: thisEnquete._id,
                title: thisEnquete.title,
                description: thisEnquete.description,
                extra: thisEnquete.extra,
                startDate: thisEnquete.startDate,
                endDate: thisEnquete.endDate,
                locations: locations,
                values: values,
                open: thisEnquete.open
            };

            return Response.success(res, data);
        })
        .then(null, next);
}


/*
URL:    /enquetes/:id
METHOD: put
PARAMS: id
QUERY:  -
BODY:   Object
{
    title: String,
    description: String,
    extra: String,
    startDate: String ('DD/MM/YYYY HH:mm'),
    endDate: String ('DD/MM/YYYY HH:mm'),
    values: [ String ],
    locations: [ String ]
}
RETURN: status
*/
function updateEnquete(req, res, next) {
    var id = req.params.id;

    var title = req.body.title;
    var description = req.body.description;
    var extra = req.body.extra;
    var startDate = moment(req.body.startDate, 'DD/MM/YYYY HH:mm');
    var endDate = moment(req.body.endDate, 'DD/MM/YYYY HH:mm');
    var values = req.body.values;
    var locations = req.body.locations;
    var open = req.body.open;

    if (!title || !description || !startDate || !endDate) {
        var msg = 'Invalid body to update enquete.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    if (endDate.diff(startDate) < 0) {
        var msg = 'Invalid body to update enquete.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    var enquete = {
        title: title,
        description: description,
        extra: extra,
        startDate: startDate,
        endDate: endDate,
        locations: locations,
        open: open
    };

    var valuesList = [];
    values.forEach(function (value) {
        valuesList.push({
            name: value.name,
            votes: []
        });
    });

    if (values) {
        enquete.values = valuesList;
    }

    Enquete.update({ _id: id }, enquete)
        .then(function (status) {
            return Response.success(res);
        })
        .then(null, next);
}


/*
URL:    /enquetes/:id
METHOD: delete
PARAMS: id
QUERY:  -
BODY:   -
RETURN: status
*/
function deleteEnquete(req, res, next) {
    var id = req.params.id;

    Enquete.findById(id)
        .remove()
        .then(function (status) {
            return Response.success(res);
        })
        .then(null, next);
}


function createFridayEnquete() {
    Template.findOne({
            repeat: true
        })
        .then(function(template) {
            if (template) {
                var enqueteValues = template.values.map(function (value) {
                    return {
                        name: value,
                        votes: []
                    };
                });

                var startDate = moment().hour(8).minute(0);
                var endDate = moment().hour(11).minute(30);

                var enquete = new Enquete({
                    title: template.title,
                    description: template.description,
                    extra: template.extra,
                    startDate: startDate,
                    endDate: endDate,
                    values: enqueteValues,
                    locations: template.locations,
                    open: template.open
                });

                enquete.save()
                    .then(function (status) {
                        console.log("FridayJob: enquete created");
                    });
            }
        });
}
