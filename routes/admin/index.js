var express = require('express');
var router = express.Router();

require('./beacons')(router)
require('./checkins')(router)
require('./enquetes')(router)
require('./notifications')(router)
require('./templates')(router)
require('./users')(router)

module.exports = router;
