var express = require('express');
var router = express.Router();
var moment = require('moment');

var Beacon = require('../../models/Beacon');
var User = require('../../models/User');
var UserEntry = require('../../models/UserEntry');

var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/checkins', getCheckinsBetween);
}

/*
URL:    /checkins
METHOD: get
PARAMS: -
QUERY:  startDate ('DD/MM/YYYY'), endDate ('DD/MM/YYYY')
BODY:   -
RETURN: Array
[
    {
        user: {
            id: String,
            name: String
        },
        location: {
            id: String,
            name: String
        },
        time: Date,
        in: Boolean
    }
]
*/
function getCheckinsBetween(req, res, next) {
    var startDate = moment().startOf('day');
    var endDate = moment().endOf('day');

    if (req.query.startDate) {
        startDate = moment(req.query.startDate, 'DD/MM/YYYY').startOf('day');
    }

    if (req.query.endDate) {
        endDate = moment(req.query.endDate, 'DD/MM/YYYY').endOf('day');
    }

    if (endDate.diff(startDate) < 0) {
        var msg = 'Parameter "startDate" must be smaller then "endDate".';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    UserEntry.find({
            time: {
                $gte: startDate.toDate(),
                $lte: endDate.toDate()
            }
        })
        .then(function (checkins) {
            return User.populate(checkins, { path: 'user' });
        })
        .then(function (checkins) {
            return Beacon.populate(checkins, { path: 'beaconId' });
        })
        .then(function (checkins) {
            var data = convertCheckins(checkins);
            return Response.success(res, data);
        })
        .then(null, next);
}

function convertCheckins(data) {
    return data.filter(function (item) {
        if (item._id && item.user) {
            return item;
        }
    })
    .map(function (item) {
        return {
            user: {
                id : item.user._id,
                email : item.user.email
            },
            location: {
                id: item.beaconId._id,
                name: item.beaconId.name
            },
            time: item.time,
            in: item.in
        };
    });
}
