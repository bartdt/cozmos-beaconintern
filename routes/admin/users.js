var express = require('express');
var router = express.Router();
var moment = require('moment');

var Beacon = require('../../models/Beacon');
var User = require('../../models/User');
var UserEntry = require('../../models/UserEntry');

var ObjectId = require('mongoose').Types.ObjectId;
var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/users', getUsers);

    router.get('/users/locations', getUsersLastLocation);
    router.get('/users/timesheets', getUsersTimesheets);

    router.get('/users/:id', getUserDetails);
    router.delete('/users/:id', deleteUser);

    router.get('/users/:id/timesheet', getUserTimesheet);
}


/*
URL:    /users
METHOD: get
PARAMS: -
QUERY:  -
BODY:   -
RETURN: Array
[
    {
        id: String,
        name: String,
        email: String,
        registrated: Boolean
    }
]
*/
function getUsers(req, res, next) {
    User.find()
        .then(function (users) {
            users = users.map(function (user) {
                return convertUser(user);
            });

            return Response.success(res, users);
        })
        .then(null, next);
}

function convertUser(user) {
    var name = user.email.split('@')[0];

    var u = {
        id: user._id,
        name: name,
        email: user.email,
        registrated: user.device.registrated
    };

    return u;
}


/*
URL:    /users/locations
METHOD: get
PARAMS: -
QUERY:  -
BODY:   -
RETURN: Array
[
    {
        id: String,
        name: String,
        location: {
            time: Date,
            in: Boolean,
            beacon: {
                id: String,
                name: String
            }
        }
    }
]
*/
function getUsersLastLocation(req, res, next) {
    UserEntry.aggregate()
        .sort({
            time: 1
        })
        .group({
            _id: '$user',
            time: { $last: '$time' },
            in: { $last: '$in' },
            beacon: { $last: '$beaconId' }
        })
        .exec()
        .then(function (userentries) {
            return User.populate(userentries, { path: '_id' });
        })
        .then(function (userentries) {
            userentries = userentries.filter(function (item) {
                if (item._id) {
                    return item;
                }
            });

            return Beacon.populate(userentries, { path: 'beacon' });
        })
        .then(function (userentries) {
            userentries = userentries.map(function (ue) {
                return convertUserEntry(ue);
            });

            return Response.success(res, userentries);
        })
        .then(null, next);
}

function convertUserEntry(userentry) {
    var name = userentry._id.email.split('@')[0];

    var ue = {
        id: userentry._id._id,
        name: name,
        location: {
            time: userentry.time,
            in: userentry.in,
            beacon: {
                id: userentry.beacon._id,
                name: userentry.beacon.name
            }
        }
    };

    return ue;
}


/*
URL:    /users/timesheets
METHOD: get
PARAMS: id
QUERY:  startDate, endDate
BODY:   -
RETURN: Array
[
    {
        id: String,
        name: String,
        currentLocation: {
            id: String,
            name: String,
            in: Boolean
        },
        totalMinutes: Number
    }
]
*/
function getUsersTimesheets(req, res, next) {
    var startDate = moment().startOf('day');
    var endDate = moment().endOf('day');

    if (req.query.startDate) {
        startDate = moment(req.query.startDate, 'DD/MM/YYYY').startOf('day');
    }

    if (req.query.endDate) {
        endDate = moment(req.query.endDate, 'DD/MM/YYYY').endOf('day');
    }

    UserEntry.aggregate()
        .match({
            time: {
                $gte: startDate.toDate(),
                $lte: endDate.toDate()
            }
        })
        .sort({
            time: 1
        })
        .group({
            _id: '$user',
            userentries: {
                $push: {
                    beacon: '$beaconId',
                    time: '$time',
                    in: '$in'
                }
            }
        })
        .exec()
        .then(function (users) {
            var list = [];
            users.forEach(function (user) {
                list.push(calculateTotalMinutes(user, endDate));
            });

            return User.populate(list, { path: 'user' });
        })
        .then(function (users) {
            users = users.filter(function (user) {
                if (user.user) {
                    return user;
                }
            })
            .map(function (user) {
                var name = user.user.email.split('@')[0];

                return {
                    id: user.user._id,
                    name: name,
                    totalMinutes: user.totalMinutes
                }
            });

            return Response.success(res, users);
        })
        .then(null, next);
}

function calculateTotalMinutes(user, endDate) {
    var obj = {};
    user.userentries.forEach(function (ue) {
        if (!obj[ue.beacon]) {
            obj[ue.beacon] = [];
        }

        obj[ue.beacon].push({
            time: ue.time,
            in: ue.in
        });
    });

    var totalMinutes = 0;
    for (prop in obj) {
        var minutes = calculateTimingForBeacon(obj[prop], endDate);
        totalMinutes += minutes;
    }

    return {
        user: user._id,
        totalMinutes: Math.round(totalMinutes)
    };
}


/*
URL:    /users/:id
METHOD: get
PARAMS: id
QUERY:  -
BODY:   -
RETURN: Object
{
    id: String,
    name: String,
    email: String,
    device: {
        id: String,
        registrated: Boolean
    },
    location: {
        time: Date,
        in: Boolean,
        beacon: {
            id: String,
            name: String
        }
    }
}
*/
function getUserDetails(req, res, next) {
    var id = req.params.id;
    var user;

    User.findById(id)
        .then(function (foundUser) {
            if (!foundUser) {
                var msg = 'User not found.'
                return Response.fail(res, msg, STATUS.notfound);
            }

            user = foundUser;
            return UserEntry.findOne({
                    user: new ObjectId(user.id)
                })
                .sort({
                    time: -1
                });
        })
        .then(function (ue) {
            return Beacon.populate(ue, { path: 'beaconId' });
        })
        .then(function (ue) {
            var userDetails = convertUserDetails(user, ue);
            return Response.success(res, userDetails);
        })
        .then(null, next);
}

function convertUserDetails(user, ue) {
    var name = user.email.split('@')[0];

    var location = null;

    if (ue) {
        location = {
            time: ue.time,
            in: ue.in,
            beacon: {
                id: ue.beaconId._id,
                name: ue.beaconId.name
            }
        }
    }

    var userDetails = {
        id: user._id,
        name: name,
        email: user.email,
        device: {
            id: user.device.id,
            registrated: user.device.registrated
        },
        location: location
    };

    return userDetails;
}


/*
URL:    /users/:id/timesheet
METHOD: get
PARAMS: id
QUERY:  startDate, endDate
BODY:   -
RETURN: Array
[
    {
        beacon: {
            id: String,
            name: String
        },
        minutes: Number
    }
]
*/
function getUserTimesheet(req, res, next) {
    var userId = req.params.id;

    var startDate = moment().startOf('day');
    var endDate = moment().endOf('day');

    if (req.query.startDate) {
        startDate = moment(req.query.startDate, 'DD/MM/YYYY').startOf('day');
    }

    if (req.query.endDate) {
        endDate = moment(req.query.endDate, 'DD/MM/YYYY').endOf('day');
    }

    UserEntry.aggregate()
        .match({
            user: new ObjectId(userId),
            time: {
                $gte: startDate.toDate(),
                $lte: endDate.toDate()
            }
        })
        .sort({
            time: 1
        })
        .group({
            _id: '$beaconId',
            userentries: {
                $push: {
                    time: '$time',
                    in: '$in'
                }
            }
        })
        .exec()
        .then(function (beacons) {
            var timesheet = [];

            beacons.forEach(function (beacon) {
                beacon.minutes = calculateTimingForBeacon(beacon.userentries, endDate);
            });

            return Beacon.populate(beacons, { path: '_id' });
        })
        .then(function (beacons) {
            var timesheet = [];

            beacons.forEach(function (beacon) {
                var t = {
                    beacon: {
                        id: beacon._id._id,
                        name: beacon._id.name
                    },
                    minutes: beacon.minutes
                };
                timesheet.push(t);
            });

            return Response.success(res, timesheet);
        })
        .then(null, next);
}

function calculateTimingForBeacon(userentries, endDate) {
    var timing = 0;

    var len = userentries.length;
    var isIn = true;
    var previousTime;
    var nextTime;

    for (var count = 0; count < len; count++) {
        var next = nextInOut(userentries, count, isIn);

        if (next) {
            count = next.count;

            if (!previousTime) {
                previousTime = next.time;
            } else {
                nextTime = next.time;

                var diff = nextTime - previousTime;
                timing += diff;

                previousTime = null;
                nextTime = null;
            }

            isIn = !isIn;
        } else {
            count = len;
        }
    }

    if (previousTime) {
        nextTime = moment();

        if (nextTime > endDate) {
            nextTime = endDate;
        }

        var diff = nextTime.toDate() - previousTime;
        timing += diff;
    }

    return timing / 1000 / 60;
}

function nextInOut(userentries, count, isIn) {
    var len = userentries.length;

    for (var i = count; i < len; i++) {
        var ue = userentries[i];

        if (ue.in === isIn) {
            return {
                time: ue.time,
                count: i
            }
        }
    }
}


/*
URL:    /users/:id
METHOD: delete
PARAMS: id
QUERY:  -
BODY:   -
RETURN: status
*/
function deleteUser(req, res, next) {
    var id = req.params.id;

    User.findById(id)
        .remove()
        .then(function (status) {
            return Response.success(res);
        })
        .then(null, next);
}
