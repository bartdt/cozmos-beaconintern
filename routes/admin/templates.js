var express = require('express');
var router = express.Router();

var Beacon = require('../../models/Beacon');
var Template = require('../../models/Template');

var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/templates', getTemplates);
    router.post('/templates', createTemplate);

    router.get('/templates/:id', getTemplate);
    router.put('/templates/:id', saveTemplate);
    router.delete('/templates/:id', deleteTemplate);
}


/*
URL:    /templates
METHOD: get
PARAMS: -
QUERY:  -
BODY:   -
RETURN: Array
[
    {
        id: String,
        title: String,
        description: String,
        values: Number,
        locations: String
    }
]
*/
function getTemplates(req, res, next) {
    Template.find()
        .then(function (templates) {
            templates = convertTemplates(templates);
            return Response.success(res, templates);
        })
        .then(null, next);
}

function convertTemplates(templates) {
    return templates.map(function (temp) {
        var len = temp.locations.length;
        return {
            id: temp._id,
            title: temp.title,
            description: temp.description,
            values: temp.values.length,
            locations: (len > 0 ? len : 'All')
        }
    })
}


/*
URL:    /templates
METHOD: post
PARAMS: -
QUERY:  -
BODY:   Object
{
    title: String,
    description: String,
    extra: String,
    values: [ String ],
    locations: [ String ]
}
RETURN: ok
*/
function createTemplate(req, res, next) {
    var title = req.body.title;
    var desc = req.body.description;
    var extra = req.body.extra;
    var values = req.body.values;
    var locations = req.body.locations || [];
    var open = req.body.open || false;

    if (!title || !desc || !values) {
        var msg = 'Invalid body to create template.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    if (values.length < 2) {
        var msg = 'Invalid body to create template.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    var template = new Template({
        title: title,
        description: desc,
        values: values,
        locations: locations,
        extra: extra,
        open: open
    });

    template.save()
        .then(function (temp) {
            return Response.success(res, null, STATUS.created);
        })
        .then(null, next);
}


/*
URL:    /templates/:id
METHOD: get
PARAMS: id
QUERY:  -
BODY:   -
RETURN: Object
{
    id: String,
    title: String,
    description: String,
    extra: String,
    values: [ String ],
    locations: [
        {
            id: String,
            name: String,
            selected: Boolean
        }
    ]
}
*/
function getTemplate(req, res, next) {
    var id = req.params.id;
    var temp;

    Template.findById(id)
        .then(function (template) {
            if (!template) {
                var msg = 'Template not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            temp = template;
            return Beacon.find();
        })
        .then(function (beacons) {
            var template = convertTemplate(temp, beacons);
            return Response.success(res, template);
        })
        .then(null, next);
}

function convertTemplate(template, beacons) {
    var bs = beacons.map(function (beacon) {
        var index = -1;
        if (template.locations.length > 0) {
            index = template.locations.indexOf(beacon._id);
        }

        return {
            id: beacon._id,
            name: beacon.name,
            selected: (index >= 0)
        };
    });

    return {
        id: template._id,
        title: template.title,
        description: template.description,
        extra: template.extra,
        values: template.values,
        locations: bs,
        open: template.open
    };
}


/*
URL:    /templates/:id
METHOD: put
PARAMS: id
QUERY:  -
BODY:   Object
{
    title: String,
    description: String,
    extra: String,
    values: [ String ],
    locations: [ String ]
}
RETURN: Object
{
    id: String,
    title: String,
    description: String,
    extra: String,
    values: [ String ],
    locations: [
        {
            id: String,
            name: String,
            selected: Boolean
        }
    ]
}
*/
function saveTemplate(req, res, next) {
    var id = req.params.id;
    var temp;

    var title = req.body.title;
    var description = req.body.description;
    var extra = req.body.extra;
    var values = req.body.values;
    var locations = req.body.locations || [];
    var open = req.body.open;

    var updateTemplate = {
        title: title,
        description: description,
        extra: extra,
        values: values,
        locations: locations,
        open: open
    };

    Template.update({ '_id': id }, updateTemplate)
        .then(function (data) {
            return Template.findById(id);
        })
        .then(function (template) {
            temp = template;
            return Beacon.find();
        })
        .then(function (beacons) {
            var template = convertTemplate(temp, beacons);
            return Response.success(res, template);
        })
        .then(null, next);
}


/*
URL:    /templates/:id
METHOD: delete
PARAMS: id
QUERY:  -
BODY:   -
RETURN: -
*/
function deleteTemplate(req, res, next) {
    var id = req.params.id;

    Template.findById(id)
        .remove()
        .then(function (status) {
            return Response.success(res);
        })
        .then(null, next);
}
