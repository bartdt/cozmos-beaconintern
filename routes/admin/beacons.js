var express = require('express');
var router = express.Router();

var Beacon = require('../../models/Beacon');
var User = require('../../models/User');
var UserEntry = require('../../models/UserEntry');

var ObjectId = require('mongoose').Types.ObjectId;
var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;

module.exports = function (router) {
    router.get('/beacons', getBeacons);
    router.post('/beacons', createBeacon);

    router.get('/beacons/:id', getBeacon);
    router.put('/beacons/:id', updateBeacon);
    router.delete('/beacons/:id', deleteBeacon);
}


/*
URL:    /admin/beacons
METHOD: get
PARAMS: -
QUERY:  -
BODY:   -
RETURN: Array
[
    {
            id: String,
            name: String,
            uuid: String,
            major: Number,
            minor: Number
    }
]
*/
function getBeacons(req, res, next) {
    Beacon.find()
        .then(function (beacons) {
            beacons = beacons.map(function (beacon) {
                return convertBeacon(beacon);
            });

            return Response.success(res, beacons);
        })
        .then(null, next);
}

function convertBeacon(beacon) {
    return {
        id: beacon._id,
        name: beacon.name,
        uuid: beacon.uuid,
        major: beacon.major,
        minor: beacon.minor
    };
}


/*
URL:    /admin/beacons
METHOD: post
PARAMS: -
QUERY:  -
BODY:   Object
{
    name: String,
    uuid: String,
    major: Number,
    minor: Number
}
RETURN: status
*/
function createBeacon(req, res, next) {
    var name = req.body.name;
    var uuid = req.body.uuid;
    var major = req.body.major;
    var minor = req.body.minor;

    if (!name || !uuid || !major || !minor) {
        var msg = 'Invalid body to create Beacon.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    var beacon = new Beacon({
        name: name,
        uuid: uuid,
        major: major,
        minor: minor
    });

    beacon.save()
        .then(function (newBeacon) {
            return Response.success(res, null, STATUS.created);
        })
        .then(null, next);
}


/*
URL:    /admin/beacons/:id
METHOD: get
PARAMS: id
QUERY:  -
BODY:   -
RETURN: Object
{
    id: String,
    name: String,
    uuid: String,
    major: Number,
    minor: Number,
    currentCheckins: [
        {
            user: {
                id: String,
                name: String
            },
            time: Date
        }
    ]
}
*/
function getBeacon(req, res, next) {
    var id = req.params.id;

    var thisBeacon;

    Beacon.findById(id)
        .then(function (beacon) {
            if (!beacon) {
                var msg = 'Beacon not found.';
                return Response.fail(res, msg, STATUS.notfound);
            }

            thisBeacon = beacon;
            return UserEntry.aggregate()
                .match({
                    beaconId: new ObjectId(id)
                })
                .sort({
                    time: -1
                })
                .group({
                    _id: '$user',
                    time: { $first: '$time' },
                    in: { $first: '$in' },
                    beaconId: { $first: '$beaconId' }
                })
                .exec();
        })
        .then(function (userentries) {
            userentries = userentries.filter(function (userEntry) {
                return userEntry.in;
            });

            return User.populate(userentries, { path: '_id' });
        })
        .then(function (userentries) {
            userentries = userentries.filter(function (userEntry) {
                return userEntry._id;
            })
            .map(function (userEntry) {
                return convertUserEntry(userEntry);
            });

            var data = convertBeacon(thisBeacon);
            data.currentCheckins = userentries;

            return Response.success(res, data);
        })
        .then(null, next);
}

function convertUserEntry(userEntry) {
    var name = userEntry._id.email.split('@')[0];

    return {
        user: {
            id: userEntry._id._id,
            name: name
        },
        time: userEntry.time
    };
}


/*
URL:    /admin/beacons/:id
METHOD: put
PARAMS: id
QUERY:  -
BODY:   Object
{
    name: String,
    uuid: String,
    major: Number,
    minor: Number
}
RETURN: status
*/
function updateBeacon(req, res, next) {
    var id = req.params.id;

    var name = req.body.name;
    var uuid = req.body.uuid;
    var major = req.body.major;
    var minor = req.body.minor;

    if (!name || !uuid || !major || !minor) {
        var msg = 'Invalid body to update Beacon.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    var beacon = {
        name: name,
        uuid: uuid,
        major: major,
        minor: minor
    };

    Beacon.update({ _id: id }, beacon)
        .then(function (status) {
            return Response.success(res);
        })
        .then(null, next);
}


/*
URL:    /admin/beacons/:id
METHOD: delete
PARAMS: id
QUERY:  -
BODY:   -
RETURN: status
*/
function deleteBeacon(req, res, next) {
    var id = req.params.id;

    Beacon.findById(id)
        .remove()
        .then(function (status) {
            return Response.success(res);
        })
        .then(null, next);
}
