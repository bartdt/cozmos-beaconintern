var express = require('express');
var router = express.Router();

var User = require('../../models/User');

var Response = require('../../utils/response');
var STATUS = require('../../config').statusCodes;
var API_KEYS = require('../../config').apiKeys;

var gcm = require('node-gcm');
var apn = require('apn');

module.exports = function (router) {
    router.post('/notifications', pushNotification);
}

/*
URL:    /admin/notifications
METHOD: post
PARAMS: -
QUERY:  -
BODY:   Object
{
    title: String,
    body: String,
    android: Boolean,
    ios: Boolean
}
RETURN: ok
*/
function pushNotification(req, res, next) {
    var title = req.body.title;
    var body = req.body.body;
    var android = req.body.android;
    var ios = req.body.ios;

    if (!title || !body || android == null || ios == null) {
        var msg = 'Invalid body.';
        return Response.fail(res, msg, STATUS.badrequest).then(null, next);
    }

    if (android) {
        sendGcm(title, body);
    }

    if (ios) {
        sendApn(title, body);
    }

    Response.success(res);
}

function sendGcm(title, body) {
    User.find({
            'device.os': 'Android'
        })
        .then(function (users) {
            var tokens = users.map(function (user) {
                return user.device.token;
            });

            var sender = new gcm.Sender(API_KEYS.gcm);

            var message = new gcm.Message();
            message.addData({
                title: title,
                body: body
            });

            sender.send(message, { registrationIds: tokens }, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    console.log(result);
                }
            });
        });
}

function sendApn(title, body) {
    User.find({
            'device.os': 'iOS'
        })
        .then(function (users) {
            var devices = users.map(function (user) {
                var token = user.device.token;
                return new apn.Device(token);
            });

            var options = {
                cert: 'cert-prod.pem',
                key: 'key-prod.pem',
                production: true
            };
            var apnConnection = new apn.Connection(options);

            apnConnection.on("transmitted", function(notification, device) {
                console.log("Notification transmitted to:" + device.token.toString("hex"));
            });

            apnConnection.on("transmissionError", function(errCode, notification, device) {
                console.error("Notification caused error: " + errCode + " for device ", device, notification);
                if (errCode === 8) {
                    console.log("A error code of 8 indicates that the device token is invalid. This could be for a number of reasons - are you using the correct environment? i.e. Production vs. Sandbox");
                }
            });

            apnConnection.on("timeout", function () {
                console.log("Connection Timeout");
            });

            apnConnection.on("socketError", console.log);

            var note = new apn.Notification();
            note.setAlertTitle(title);
            note.setAlertText(body);

            var optionsFeedback = {
                "batchFeedback": true,
                "interval": 300
            };

            var feedback = new apn.Feedback(optionsFeedback);
            feedback.on("feedback", function(devices) {
                devices.forEach(function(item) {
                    console.log(item);
                });
            });

            devices.forEach(function (device) {
                apnConnection.pushNotification(note, device);
            });
        });
}
