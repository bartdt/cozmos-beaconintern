var express = require('express');
var router = express.Router();

var User = require('../models/User');

router.get('/', function (req, res, next) {
    var deviceId = req.query.deviceId;
    var email = req.query.email;

    if (!deviceId && !email) {
        return res.send('Invalid code combination ...');
    }

    User.findOne({ 'device.id': deviceId, email: email })
        .then(function (user) {
            if (!user) {
                return res.send('Invalid code combination ...');
            }

            user.device.registrated = true;
            return user.save();
        })
        .then(function (updatedUser) {
            if (updatedUser) {
                return res.send('Registration successful!');
            }
        })
        .then(null, next);
});

module.exports = router;
