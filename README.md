Setting up the project locally
====================
* Install Node.js

* Download MongoDb

Advice:
-------------
* Add the mongoDb directory to your $PATH

* make the default data folder for mongoDb:

	In Terminal:

		mkdir /data/db
		chown -R $USER /data/db

Install modules
-------------
Open terminal and navigate to the project directory then do:

	npm install

Start mongoDb:
-------------
If you added MongoDb to your $PATH and made the default data directory
	In terminal:

	mongod

Start the server:
-------------
Open terminal and navigate to the project directory then do:

	node app.js
	
