var MongooseUtils = require('../utils/mongoose-utils');

var EnqueteValue = MongooseUtils.schema({
    name: String,
    votes: [ MongooseUtils.id('User') ]
});

var Enquete = MongooseUtils.schema({
    title: String,
    description: String,
    extra: String,
    startDate: Date,
    endDate: Date,
    values: [ EnqueteValue ],
    locations: [ MongooseUtils.id('Beacon') ],
    open: Boolean
});

module.exports = MongooseUtils.model('Enquete', Enquete);
