var MongooseUtils = require('../utils/mongoose-utils');

var Template = MongooseUtils.schema({
    title: String,
    description: String,
    values: [ String ],
    locations: [ MongooseUtils.id('Beacon') ],
    extra: String,
    open: Boolean,
    repeat: {
        type: Boolean,
        default: false
    }
});

module.exports = MongooseUtils.model('Template', Template);
