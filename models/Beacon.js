var MongooseUtils = require('../utils/mongoose-utils');

var Beacon = MongooseUtils.schema({
    uuid: String,
    major: Number,
    minor: Number,
    name: String
});

module.exports = MongooseUtils.model('Beacon', Beacon);
