var MongooseUtils = require('../utils/mongoose-utils');

var User = MongooseUtils.schema({
    device: {
        name: String,
        id: String,
        registrated: Boolean,
        os: String,
        token: String
    },
    email: String
});

module.exports = MongooseUtils.model('User', User);
