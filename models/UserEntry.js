var MongooseUtils = require('../utils/mongoose-utils');

var UserEntry = MongooseUtils.schema({
    user: MongooseUtils.id('User'),
    in: Boolean,
    time: MongooseUtils.date(),
    beaconId: MongooseUtils.id('Beacon')
});

module.exports = MongooseUtils.model('UserEntry', UserEntry);
